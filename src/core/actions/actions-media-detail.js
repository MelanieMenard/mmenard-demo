/* ------------------------------------------- */
/*   Actions for fetching media from API
/*   Described what happened in the UI/App Logic to Redux
/*   Then the reducers decide whether the action should result in a state update
/*   Follow the Flux Standard Action format that wraps the properties inside 'payload' object, as I find it neater
/*   https://github.com/redux-utilities/flux-standard-action
/* ------------------------------------------- */

import { getFlickrPhotoInfoQuery } from 'AppSrc/core/queries/axios-queries';


/* --- Actions Types --- */
// Defining actions types as constants rather than strings make the error picked up earlier if you try to dispatch an action that does not exist
// it throws an undefined constant error immediately instead of dispatching a non existing action through the system that just flows though without triggering any reducer.

// Fetch media detail from API
const FETCH_MEDIA_DETAIL_REQUEST = 'FETCH_MEDIA_DETAIL_REQUEST';
const FETCH_MEDIA_DETAIL_SUCCESS = 'FETCH_MEDIA_DETAIL_SUCCESS';
const FETCH_MEDIA_DETAIL_ERROR = 'FETCH_MEDIA_DETAIL_ERROR';


/* ------ ACTION CREATORS ------ */

/* --- Fetch media items ACTIONS --- */

/* - Fetch media items synchronous actions - */

const fetchMediaDetailRequest = (mediaId) => ({
  type: FETCH_MEDIA_DETAIL_REQUEST,
  payload: {
    mediaId,
  },
});

const fetchMediaDetailSuccess = (mediaId, mediaData) => ({
  type: FETCH_MEDIA_DETAIL_SUCCESS,
  payload: {
    mediaId,
    mediaData,
  },
});

const fetchMediaDetailError = (mediaId, error) => ({
  type: FETCH_MEDIA_DETAIL_ERROR,
  payload: {
    mediaId,
    error,
  },
});


/* - Fetch media detail asynchronous action made by combining synchronous actions  - */
/* - Thunk Actions replace controllers in functional programming Redux apps - */
/* - Thunk Actions are the place to do all 'impure' business logic such as fetching data and routing - */
const fetchMediaDetail = (mediaId) => (dispatch, getState) => {

  // notify reducer fetching has started
  dispatch(fetchMediaDetailRequest(mediaId));

  // REST get request
  return getFlickrPhotoInfoQuery(mediaId)
    .then((response) => {
      dispatch(fetchMediaDetailSuccess(mediaId, response));
    })
    .catch((error) => {
      dispatch(fetchMediaDetailError(mediaId, error));
    });
};


export {
  fetchMediaDetailRequest,
  fetchMediaDetailSuccess,
  fetchMediaDetailError,
  fetchMediaDetail,
  FETCH_MEDIA_DETAIL_REQUEST,
  FETCH_MEDIA_DETAIL_SUCCESS,
  FETCH_MEDIA_DETAIL_ERROR,
};
