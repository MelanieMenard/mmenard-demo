/* ------------------------------------------- */
/*   Actions for fetching media from API
/*   Described what happened in the UI/App Logic to Redux
/*   Then the reducers decide whether the action should result in a state update
/*   Follow the Flux Standard Action format that wraps the properties inside 'payload' object, as I find it neater
/*   https://github.com/redux-utilities/flux-standard-action
/* ------------------------------------------- */

import {
  getFlickrSearchQuery,
  MEDIA_PER_PAGE,
} from 'AppSrc/core/queries/axios-queries';
import { calculateTagsPopularity } from 'AppSrc/core/actions/actions-tags';


/* --- Actions Types --- */
// Defining actions types as constants rather than strings make the error picked up earlier if you try to dispatch an action that does not exist
// it throws an undefined constant error immediately instead of dispatching a non existing action through the system that just flows though without triggering any reducer.

// Fetch media items from API
const FETCH_MEDIA_LIST_REQUEST = 'FETCH_MEDIA_LIST_REQUEST';
const FETCH_MEDIA_LIST_SUCCESS = 'FETCH_MEDIA_LIST_SUCCESS';
const FETCH_MEDIA_LIST_ERROR = 'FETCH_MEDIA_LIST_ERROR';


/* ------ ACTION CREATORS ------ */

/* --- Fetch media items ACTIONS --- */

/* - Fetch media items synchronous actions - */

const fetchMediaListRequest = (tagId, page) => ({
  type: FETCH_MEDIA_LIST_REQUEST,
  payload: {
    tagId,
    page,
  },
});

const fetchMediaListSuccess = (tagId, items, totalItems, currentPage, totalPages, perPage) => ({
  type: FETCH_MEDIA_LIST_SUCCESS,
  payload: {
    tagId,
    items,
    totalItems,
    currentPage,
    totalPages,
    perPage,
  },
});

const fetchMediaListError = (tagId, page, error) => ({
  type: FETCH_MEDIA_LIST_ERROR,
  payload: {
    tagId,
    page,
    error,
  },
});


/* - Fetch a page of media items for a specific tag 'thunk' asynchronous action made by combining synchronous actions  - */
/* - Thunk Actions replace controllers in functional programming Redux apps - */
/* - Thunk Actions are the place to do all 'impure' business logic such as fetching data and routing - */
const fetchMediaPageByTag = (tagId, page = 1, perPage = MEDIA_PER_PAGE) => (dispatch, getState) => {

  const tags = getState().tags;
  const searchString = tags.locationsById[tagId].searchQuery;

  // notify reducer fetching has started
  dispatch(fetchMediaListRequest(tagId, page));

  // REST get request
  return getFlickrSearchQuery(searchString, page, perPage)
    .then((response) => {
      const totalItems = response.total;
      const items = response.items;
      const totalPages = response.pages;
      const currentPage = response.page;

      dispatch(fetchMediaListSuccess(tagId, items, totalItems, currentPage, totalPages, perPage));
      return response;
    })
    .catch((error) => {
      dispatch(fetchMediaListError(tagId, page, error));
    });
};


/* - Fetch all pages of media items for a specific tag 'thunk' asynchronous action made by combining synchronous actions  - */
const fetchMediaByTag = (tagId, page = 1, perPage = MEDIA_PER_PAGE) => (dispatch, getState) => {

  dispatch(fetchMediaPageByTag(tagId, page, perPage))
    .then((response) => {
      const fetchedPage = response.page;
      const totalPages = response.pages;
      const fetchNextPage = (fetchedPage < totalPages);
      console.log(`fetchMediaByTag FOR TAG ${tagId} FETCHED: ${fetchedPage} OUT OF ${totalPages} FETCH NEXT PAGE? ${fetchNextPage}`);
      if (fetchNextPage) {
        // fetch next page, the action dispatches itself with incremented page
        dispatch(fetchMediaByTag(tagId, fetchedPage + 1));
      }
    });
};


/* - Fetch first page of media items for all locations in the tag list: 'thunk' action that dispatches other thunk actions  - */
const fetchMediaAllTags = () => (dispatch, getState) => {

  const tags = getState().tags;

  Promise.all(tags.allLocations.map((tagId) => dispatch(fetchMediaPageByTag(tagId))))
    .then((response) => {
      // now that we have items count for each tag, calculate their relative popularity
      dispatch(calculateTagsPopularity());
    });
};


export {
  fetchMediaListRequest,
  fetchMediaListSuccess,
  fetchMediaListError,
  fetchMediaPageByTag,
  fetchMediaByTag,
  fetchMediaAllTags,
  FETCH_MEDIA_LIST_REQUEST,
  FETCH_MEDIA_LIST_SUCCESS,
  FETCH_MEDIA_LIST_ERROR,
};
