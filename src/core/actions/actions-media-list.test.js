import {
	fetchMediaListRequest,
  fetchMediaListSuccess,
  fetchMediaListError,
  fetchMediaPageByTag,
  FETCH_MEDIA_LIST_REQUEST,
  FETCH_MEDIA_LIST_SUCCESS,
  FETCH_MEDIA_LIST_ERROR,
} from 'AppSrc/core/actions/actions-media-list';
import {  
  mockStore,
  testSynchronousActionCreator,
} from 'AppSrc/core/test/utils/action-test-utils';
import { testError } from 'AppSrc/core/test/fixtures/error-fixture';
import {
  media1,
  mediaListPage1,
  TOTAL_MEDIA_ITEMS_ON_SERVER,
  TOTAL_MEDIA_PAGES_ON_SERVER,
  MEDIA_PER_PAGE_FIXTURE,
} from 'AppSrc/core/test/fixtures/media-list-fixtures';
import { getFlickrSearchQuery } from 'AppSrc/core/queries/axios-queries';
import { mediaListReducerDefaultState } from 'AppSrc/core/reducers/reducer-media-list';
import { tagsDefaultState } from 'AppSrc/core/reducers/reducer-tags';


const testMediaListSynchronousActions = () => {

  describe('media list synchronous action creators', () => {

    testSynchronousActionCreator(
      fetchMediaListRequest,
      FETCH_MEDIA_LIST_REQUEST,
      {
        tagId: 'monksHouse',
        page: 1,
      }
    );

    testSynchronousActionCreator(
      fetchMediaListSuccess,
      FETCH_MEDIA_LIST_SUCCESS,
      {
        tagId: 'monksHouse',
        items: mediaListPage1,
        totalItems: TOTAL_MEDIA_ITEMS_ON_SERVER,
        currentPage: 1,
        totalPages: TOTAL_MEDIA_PAGES_ON_SERVER,
        perPage: MEDIA_PER_PAGE_FIXTURE,
      }
    );

    testSynchronousActionCreator(
      fetchMediaListError,
      FETCH_MEDIA_LIST_ERROR,
      {
        tagId: 'monksHouse',
        page: 1,
        error: testError,
      }
    );
  });
}

// Mock the axios query imported in the async action
jest.mock('AppSrc/core/queries/axios-queries', () => ({
  getFlickrSearchQuery: jest.fn(() => true),
}));


const testMediaListAsyncThunkActions = () => {

  describe('media list async thunk actions', () => {

    it('fetchMediaPageByTag dispatches request and success', () => {

      const store = mockStore({ 
        mediaList: mediaListReducerDefaultState,
        tags: tagsDefaultState,
      });

      // mock a standard array of media items as return value from the API
      getFlickrSearchQuery.mockResolvedValue({
        items: mediaListPage1,
        total: TOTAL_MEDIA_ITEMS_ON_SERVER,
        pages: TOTAL_MEDIA_PAGES_ON_SERVER,
        page: 1,
        perPage: MEDIA_PER_PAGE_FIXTURE,
      });

      // expect.assertions(expectedAssertionsCount) tells Jest how many assertations should be tested in async code
      expect.assertions(2);

      // dispatch the async action into the mock store
      return store.dispatch(fetchMediaPageByTag('monksHouse', 1, MEDIA_PER_PAGE_FIXTURE)).then(() => {

        // expect the query to have been called with the tag's search string
        expect(getFlickrSearchQuery).toHaveBeenCalledWith(store.getState().tags.locationsById['monksHouse'].searchQuery, 1, MEDIA_PER_PAGE_FIXTURE);
  
        const expectedActions = [
          { 
            type: 'FETCH_MEDIA_LIST_REQUEST',
            payload: {
              tagId: 'monksHouse',
              page: 1,
            }
          },
          {
            type: 'FETCH_MEDIA_LIST_SUCCESS',
            payload: {
              tagId: 'monksHouse',
              items: mediaListPage1,
              totalItems: TOTAL_MEDIA_ITEMS_ON_SERVER,
              currentPage: 1,
              totalPages: TOTAL_MEDIA_PAGES_ON_SERVER,
              perPage: MEDIA_PER_PAGE_FIXTURE,
            }
          }
        ];

        // expect the request and success actions to have been dispatched
        const actions = store.getActions();
        expect(actions).toEqual(expectedActions);
      });   
    });

    it('fetchMediaPageByTag dispatches request and error', () => {

      const store = mockStore({ 
        mediaList: mediaListReducerDefaultState,
        tags: tagsDefaultState,
      });

      // mock a standard error items as return value from the API
      getFlickrSearchQuery.mockRejectedValue(testError);

      expect.assertions(2);

      // dispatch the async action into the mock store
      return store.dispatch(fetchMediaPageByTag('monksHouse', 1, MEDIA_PER_PAGE_FIXTURE)).then(() => {

        // expect the query to have been called with the tag's search string
        expect(getFlickrSearchQuery).toHaveBeenCalledWith(store.getState().tags.locationsById['monksHouse'].searchQuery, 1, MEDIA_PER_PAGE_FIXTURE);

        const expectedActions = [
          { 
            type: 'FETCH_MEDIA_LIST_REQUEST',
            payload: {
              tagId: 'monksHouse',
              page: 1,
            }
          },
          {
            type: 'FETCH_MEDIA_LIST_ERROR',
            payload: {
              tagId: 'monksHouse',
              page: 1,
              error: testError,
            }
          }
        ];

        // expect the request and success actions to have been dispatched
        const actions = store.getActions();
        expect(actions).toEqual(expectedActions);
      });   
    });
  });

}

// run the actual tests
describe('media list actions', () => {
  testMediaListSynchronousActions();
  testMediaListAsyncThunkActions();
});
