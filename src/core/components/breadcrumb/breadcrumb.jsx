import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import './breadcrumb.css';

const Breadcrumb = React.memo(({ breadcrumbs }) => (
  <ul className="breadcrumbs">
    {breadcrumbs.map((breadcrumb, index) => (
      <li className="breadcrumb" key={breadcrumb.link}>
        { (index > 0) && (
          <span className="breadcrumb-separator">&gt;</span>
        )}
        <Link className="breadcrumb-link" to={breadcrumb.link}>{breadcrumb.title}</Link>
      </li>
    ))}
  </ul>
));

const breadcrumbPropTypesShape = PropTypes.shape({
  link: PropTypes.string,
  title: PropTypes.string,
});

Breadcrumb.propTypes = {
  breadcrumbs: PropTypes.arrayOf(breadcrumbPropTypesShape),
};

export { Breadcrumb };
