import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import { TagCloud } from 'AppSrc/core/components/tag-cloud/tag-cloud';
import { LocationMap } from 'AppSrc/core/components/location-map/location-map';
import { FilteredMediaList } from 'AppSrc/core/components/filtered-media-list/filtered-media-list';
import { locationPropTypesShape } from 'AppSrc/core/data-structures/location-proptypes';
import { LocationDetailLink } from 'AppSrc/core/components/location-detail-link/location-detail-link';
import { selectedTagSelector } from 'AppSrc/core/selectors/selectors-tags';


const HomeScreenDisplay = React.memo(({ selectedLocation }) => (
  <Fragment>
    <div className="app-full-width-content">
      <LocationMap />
    </div>
    <div className="app-sidebar">
      <TagCloud />
    </div>
    <div className="app-main-content">
      {selectedLocation && (
        <LocationDetailLink
          location={selectedLocation}
        />
      )}
      <FilteredMediaList />
    </div>
  </Fragment>
));

HomeScreenDisplay.propTypes = {
  selectedLocation: locationPropTypesShape,
};


/* --- HomeScreen container component --- */

// the parent components can still pass props directly to the presentational component, and mapStateToProps can access them as ownProps
// the presentational component does not know whether its props come from the parent (ownProps) or the container, the difference is only visible in mapStateToProps
const mapStateToProps = (state, ownProps) => ({
  selectedLocation: selectedTagSelector(state),
});

const HomeScreen = connect(
  mapStateToProps,
)(HomeScreenDisplay);


export {
  HomeScreenDisplay,
  HomeScreen,
};
