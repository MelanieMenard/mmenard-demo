/* --- Lazy loader component --- */
// lazyLoad: function that will getch a page of data when lazyloader detects it is necessary. takes as input lazyLoadParams below
// lazyLoadParams: parameters for the paginated request to fetch a page of data
// fullyLoaded: true if all items have been fetched from server
// bottomOffset = allow positive or negative space at the bottom of the screen


import React, { PureComponent } from 'react';
import { PropTypes } from 'prop-types';

const paginationShape = PropTypes.shape({
  page: PropTypes.number.isRequired,
  perPage: PropTypes.number.isRequired,
});

const lazyLoadParamsShape = PropTypes.shape({
  paginationData: paginationShape,
  isFetching: PropTypes.bool.isRequired,
  params: PropTypes.any,
});


class LazyLoader extends PureComponent {

  constructor(props) {
    super(props);
    this.previousLazyLoadData = null;
    this.shouldILoad = this.shouldILoad.bind(this);
    this.lazyLoad = this.lazyLoad.bind(this);
  }

  componentDidMount() {
    this.setScrollEvent();
    // gets first page data and then checks if needs more to fill the screen
    this.shouldILoad();
  }

  componentDidUpdate() {
    this.shouldILoad();
  }

  componentWillUnmount() {
    this.unsetScrollEvent();
  }

  // listen to scroll events
  setScrollEvent() {
    document.addEventListener('scroll', this.shouldILoad);
  }

  unsetScrollEvent() {
    document.removeEventListener('scroll', this.shouldILoad);
  }

  // fetch a page of data
  lazyLoad() {
    const {
      lazyLoadParams,
    } = this.props;

    if (lazyLoadParams.isFetching) {
      // no ops if it's already fetching
      return null;
    }

    const previousLazyLoadDataJSON = JSON.stringify(this.previousLazyLoadData);
    const lazyLoadParamsJSON = JSON.stringify(lazyLoadParams);

    // Blocks multiple identical requests
    if (previousLazyLoadDataJSON === lazyLoadParamsJSON) {
      // no ops if the request has the same paramaters as the last
      return null;
    }

    this.props.lazyLoad(lazyLoadParams);
    this.previousLazyLoadData = lazyLoadParams;
    return null;
  }

  // check whether we need to fetch more items to fill the visible screen
  shouldILoad() {

    const {
      bottomOffset,
      fullyLoaded,
    } = this.props;

    // stops trying to make requests when all the data has been fetched
    if (fullyLoaded) {
      this.unsetScrollEvent();
      return null;
    }

    // height scrollable document is
    const scrollHeight = document.documentElement.scrollHeight;
    // height currently visisble in the browser
    const offsetHeight = document.documentElement.offsetHeight;
    // top of the viewable screen
    const scrollTop = document.documentElement.scrollTop;
    // bottom of the viewable screen (the browser doesn't give us this so we calculate it ourself)
    const scrollBottom = scrollTop + offsetHeight;

    const scrolledToBottomOfDocument = (scrollBottom + bottomOffset) >= scrollHeight;

    if (scrolledToBottomOfDocument) {
      this.lazyLoad();
    }
    return null;
  }

  render() {
    const { children } = this.props;
    return (
      <React.Fragment>
        {children}
      </React.Fragment>
    );
  }
}

LazyLoader.propTypes = {
  lazyLoad: PropTypes.func,
  lazyLoadParams: lazyLoadParamsShape,
  fullyLoaded: PropTypes.bool,
  children: PropTypes.node,
  bottomOffset: PropTypes.number,
};

LazyLoader.defaultProps = {
  // increase or decrease the point at which lazyLoad should be fetched
  bottomOffset: 0,
};

export { LazyLoader, lazyLoadParamsShape };
