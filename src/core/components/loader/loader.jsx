import React from 'react';
import PropTypes from 'prop-types';
import './loader.css';

const Loader = React.memo(({ loaderStyle }) => {
  const loaderStyleClass = loaderStyle || 'spinning';
  const className = `loader ${loaderStyleClass}`;
  return (
    <div className={className} />
  );
});

Loader.propTypes = {
  loaderStyle: PropTypes.string,
};

export { Loader };
