import React from 'react';
import { mount } from 'enzyme';
import {
  rendersWithoutCrashing,
  matchesThePreviousSnapshot,
} from 'AppSrc/core/test/utils/component-test-utils';
import { Loader } from './loader';

describe('Loader Component', () => {
  rendersWithoutCrashing(<Loader />);
  matchesThePreviousSnapshot(<Loader />, {shallow: true});
});