import React from 'react';
import { Link } from 'react-router-dom';
import { locationPropTypesShape } from 'AppSrc/core/data-structures/location-proptypes';
import './location-detail-link.css';

const LocationDetailLink = React.memo(({ location }) => (
  <p className="location-detail-more">
    <Link to={`/location/${location.id}`} className="location-detail-link">
      {`More on ${location.displayName} `}
      &rarr;
    </Link>
  </p>
));

LocationDetailLink.propTypes = {
  location: locationPropTypesShape,
};

export { LocationDetailLink };
