import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import ReactRouterPropTypes from 'react-router-prop-types';
import { locationPropTypesShape } from 'AppSrc/core/data-structures/location-proptypes';
import { Breadcrumb } from 'AppSrc/core/components/breadcrumb/breadcrumb';
import { LocationMediaListLazyLoad } from 'AppSrc/core/components/location-media-list-lazyload/location-media-list-lazyload';
import { makeGetTagById } from 'AppSrc/core/selectors/selectors-tags';
import './location-detail.css';


const locationDetailScreenBreadcrumbs = [
  {
    link: '/',
    title: 'Location map',
  },
];


const LocationDetailDisplay = React.memo(({ match, location }) => (
  <div className="location-detail">
    <Breadcrumb
      breadcrumbs={locationDetailScreenBreadcrumbs}
    />
    <h2 className="location-detail-title">{location.displayName}</h2>
    <p className="location-detail-description">{location.description}</p>
    <LocationMediaListLazyLoad
      locationId={location.id}
    />
  </div>
));


LocationDetailDisplay.propTypes = {
  match: ReactRouterPropTypes.match.isRequired,
  location: locationPropTypesShape,
};


/* --- Location detail container component --- */


// MM: 'makeMapStateToProps' is so each instance of the container gets its own mapStateToProps
// otherwise the selectors that depend on props don't properly memoize
// https://redux.js.org/recipes/computing-derived-data#sharing-selectors-across-multiple-components
const makeMapStateToProps = () => {

  const getTagById = makeGetTagById();

  const mapStateToProps = (state, ownProps) => ({
    location: getTagById(state, ownProps.match.params.locationId),
  });
  return mapStateToProps;
};

const LocationDetail = withRouter(connect(
  makeMapStateToProps,
)(LocationDetailDisplay));


export {
  LocationDetailDisplay,
  LocationDetail,
};
