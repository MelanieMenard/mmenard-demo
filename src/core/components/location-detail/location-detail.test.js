import React from 'react';
import { mount } from 'enzyme';
import {
  TestRouter,
  rendersWithoutCrashing,
  matchesThePreviousSnapshot,
} from 'AppSrc/core/test/utils/component-test-utils';
import { tag1 } from 'AppSrc/core/test/fixtures/tag-fixtures';
import { LocationDetailDisplay } from './location-detail';

// mock out the LocationMediaListLazyLoad component to keep the snapshot small
// we can't use 'shallow' because LocationMediaListLazyLoad needs to have a Provider in its surrounding context
// When a component needs to be wrapped in a Provider or Router, we can't use shallow, we need to mock them instead
jest.mock('AppSrc/core/components/location-media-list-lazyload/location-media-list-lazyload', () => (
  { LocationMediaListLazyLoad: 'mock-LocationMediaListLazyLoad' }
));

jest.mock('AppSrc/core/components/breadcrumb/breadcrumb', () => (
  { Breadcrumb: 'mock-breadcrumb' }
));

const LocationDetailWithRouter = (
  <TestRouter>
    <LocationDetailDisplay
      location={tag1}
      match={{params: {locationId: tag1.id}, isExact: true, path: "", url: ""}}
    />
  </TestRouter>
);

const LocationDetailWithData = (
  <LocationDetailDisplay
    location={tag1}
    match={{params: {locationId: tag1.id}, isExact: true, path: "", url: ""}}
  />
);


describe('LocationDetail Component', () => {
  rendersWithoutCrashing(LocationDetailWithRouter);
  matchesThePreviousSnapshot(LocationDetailWithData, {shallow: true});
});