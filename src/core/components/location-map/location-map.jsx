import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import * as d3 from 'd3';
import {
  locationPropTypesShape,
  colorMapPropTypesShape,
} from 'AppSrc/core/data-structures/location-proptypes';
import {
  colorMapSelector,
  selectedTagIdSelector,
  allLocationsSelector,
} from 'AppSrc/core/selectors/selectors-tags';
import './location-map.css';

class LocationMapDisplay extends React.PureComponent {

  constructor(props) {
    super(props);

    this.state = {
      chartWidth: 0,
      chartHeight: 0,
    };
    this.containerEl = React.createRef();
    this.svgEl = React.createRef();
    this.initChartDimensions = this.initChartDimensions.bind(this);
  }

  componentDidMount() {
    this.initChartDimensions();
    window.addEventListener('resize', this.initChartDimensions);
  }

  componentDidUpdate() {
    this.updateChart();
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.initChartDimensions);
  }

  // dynamically init canvas width based on screen size
  // https://stackoverflow.com/questions/36862334/get-viewport-window-height-in-reactjs
  // https://stackoverflow.com/questions/49058890/how-to-get-a-react-components-size-height-width-before-render
  initChartDimensions() {

    const chartWidth = Math.min(this.containerEl.current.clientWidth, 1024);
    const chartHeight = Math.round(chartWidth * 2 / 3);
    console.log('initChartDimensions: ', chartWidth, chartHeight);

    this.setState({
      chartWidth,
      chartHeight,
    });
  }

  updateChart() {

    const { chartWidth, chartHeight } = this.state;
    const { colorMap, selectedLocation, locations } = this.props;

    const latitudes = locations.map((location) => location.latitude).sort();
    const longitudes = locations.map((location) => location.longitude).sort();

    const minLat = latitudes.length ? latitudes[0] : 0;
    const maxLat = latitudes.length ? latitudes[latitudes.length - 1] : 0;
    const minLon = longitudes.length ? longitudes[0] : 0;
    const maxLon = longitudes.length ? longitudes[latitudes.length - 1] : 0;
    const rangeLat = maxLat - minLat;
    const rangeLon = maxLon - minLon;
    const centerLat = (maxLat + minLat) / 2;
    const centerLon = (maxLon + minLon) / 2;

    // calculate scale for the map
    // best scale that would fit the longitude range on the width of the image
    // whole world has a longitude range of 360, from -180 to +180
    const scaleLon = Math.round((360 * chartWidth) / (maxLon - minLon));
    // best scale that would fit the latitude range on the height of the image
    // whole world has a latitude range of 180, from -90 to +90
    const scaleLat = Math.round((180 * chartHeight) / (maxLat - minLat));

    // MM: mapZoom is a mystery coefficient that seems to make most range of lat/log fit nicely on screen
    // 8 works best for 'worldwide' type data where the range is > 100
    // otherwise (for Europe, country or city type data) 7 works best
    // I have no idea where these numbers come from, or the math behind them, found by trial and error
    const mapRange = Math.round(Math.max(rangeLat, rangeLon));
    const mapZoom = (mapRange > 100) ? 8 : 7;
    const mapScale = Math.round(Math.min(scaleLon, scaleLat) / mapZoom);

    const center = [centerLon, centerLat];

    const projection = d3.geoMercator()
      .center(center)
      .scale(mapScale)
      .translate([chartWidth / 2, chartHeight / 2]);

    // Follows d3 general update pattern:
    // https://www.d3indepth.com/enterexit/#general-update-pattern
    // And tuturial to integrate D3 in react components
    // https://frontendcharts.com/react-d3-integrate/
    // https://codepen.io/frontendcharts/pen/NBYLvy
    // http://jsfiddle.net/3bzsE/
    // https://bl.ocks.org/d3indepth/e890d5ad36af3d949f275e35b41a99d6

    // get the svg ref from react
    const svg = d3.select(this.svgEl.current);

    // Define the data for the locations, and use id as unique key
    const location = svg.selectAll('.location')
      .data(locations, (d) => d.id);

    // Create and place the "blocks" containing the circle and the text
    const locationEnter = location.enter()
      .append('g')
      .classed('location', true)
      .classed('selected', (d) => ((d.id === selectedLocation)))
      .attr('id', (d) => `location-${d.id}`);

    // create the location circles with initial style inside each block
    locationEnter.append('circle')
      .classed('location-circle', true)
      .attr('r', 1)
      .attr('cx', chartWidth / 2)
      .attr('cy', chartHeight / 2)
      .attr('fill', '#fff')
      .attr('fill-opacity', 0);

    // create the location labels with initial style inside each block
    locationEnter.append('text')
      .classed('location-label', true)
      .style('text-anchor', 'start')
      .text((d) => d.displayName)
      .attr('font-size', '11px')
      .attr('fill', (d) => (colorMap[d.type] || colorMap.other))
      .attr('dx', chartWidth / 2)
      .attr('dy', chartHeight / 2);

    // add class selected when the data updates
    locationEnter
      .merge(location)
      .classed('selected', (d) => ((d.id === selectedLocation)));

    // move selected element to the top (D3 svg version of z-index)
    svg.select('.location.selected')
      .raise();

    // animate the location circles when the data updates
    locationEnter
      .merge(location)
      .select('.location-circle')
      .transition()
      .duration(1000)
      .attr('cx', (d) => projection([d.longitude, d.latitude])[0])
      .attr('cy', (d) => projection([d.longitude, d.latitude])[1])
      .attr('r', (d) => d.popularityWideScale)
      .attr('fill', (d) => (colorMap[d.type] || colorMap.other))
      .attr('fill-opacity', (d) => ((d.id === selectedLocation) ? 1 : 0.7));

    // animate the location labels when the data updates
    locationEnter
      .merge(location)
      .select('.location-label')
      .transition()
      .duration(1000)
      .attr('font-size', (d) => ((d.id === selectedLocation) ? '18px' : '11px'))
      .attr('dx', (d) => (projection([d.longitude, d.latitude])[0] + ((d.id === selectedLocation) ? 18 : 10)))
      .attr('dy', (d) => (projection([d.longitude, d.latitude])[1] - 6));

    // remove the outdated elements
    location.exit().remove();

  }

  renderChart() {
    const { chartWidth, chartHeight } = this.state;
    return (
      <svg className="location-map-svg" width={chartWidth} height={chartHeight} ref={this.svgEl} />
    );
  }

  render() {
    const { chartWidth, chartHeight } = this.state;
    return (
      <div className="location-map" ref={this.containerEl}>
        {chartWidth && chartHeight && this.renderChart()}
      </div>
    );
  }
}

LocationMapDisplay.propTypes = {
  locations: PropTypes.arrayOf(locationPropTypesShape),
  selectedLocation: PropTypes.string,
  colorMap: colorMapPropTypesShape,
};


/* --- Location Map container component --- */
// https://redux.js.org/basics/usage-with-react#implementing-container-components
// the container component is generated by react-redux
// it feeds the data from the redux store to the presentational component


// the parent components can still pass props directly to the presentational component, and mapStateToProps can access them as ownProps
// the presentational component does not know whether its props come from the parent (ownProps) or the container, the difference is only visible in mapStateToProps
const mapStateToProps = (state, ownProps) => ({
  locations: allLocationsSelector(state),
  selectedLocation: selectedTagIdSelector(state),
  colorMap: colorMapSelector(state),
});

// mapDispatchToProps tells the container component how to dispatch actions to the redux store
// LocationMap does not dispatch any action so mapDispatchToProps is null
const LocationMap = connect(
  mapStateToProps,
  null,
)(LocationMapDisplay);


// 'Display' unconnected presentational components are exported for testing only
export {
  LocationMapDisplay,
  LocationMap,
};
