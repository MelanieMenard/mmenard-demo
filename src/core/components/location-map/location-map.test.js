import React from 'react';
import { mount } from 'enzyme';
import { rendersWithoutCrashing, } from 'AppSrc/core/test/utils/component-test-utils';
import {
  tagsArray,
  colorMap,
} from 'AppSrc/core/test/fixtures/tag-fixtures';
import { LocationMapDisplay } from './location-map';

const UnconnectedLocationMap = (<LocationMapDisplay
  locations={tagsArray}
  colorMap={colorMap}
  selectedLocation={null}
/>);

/* no snapshot test for d3 components as d3 manipulates the DOM */
describe('Unconnected Location Map Component', () => {
  rendersWithoutCrashing(UnconnectedLocationMap);
});