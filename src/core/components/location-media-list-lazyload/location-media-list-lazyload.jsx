import { connect } from 'react-redux';
import { MediaListLazyLoad } from 'AppSrc/core/components/media-list-lazyload/media-list-lazyload';
import { fetchMediaPageByTag } from 'AppSrc/core/actions/actions-media-list';
import {
  makeMediaByLocationIdItems,
  makeMediaByLocationIdIsFetched,
  makeMediaByLocationIdFullyLoaded,
  makeMediaByLocationIdLazyLoadParams,
} from 'AppSrc/core/selectors/selectors-media-list';


/* --- Location Media List with LazyLoad container component --- */

// MM: 'makeMapStateToProps' is so each instance of the container gets its own mapStateToProps
// otherwise the selectors that depend on props don't properly memoize
// https://redux.js.org/recipes/computing-derived-data#sharing-selectors-across-multiple-components
const makeMapStateToProps = () => {

  const getMediaByLocationIdItems = makeMediaByLocationIdItems();
  const getMediaByLocationIdIsFetched = makeMediaByLocationIdIsFetched();
  const getMediaByLocationIdFullyLoaded = makeMediaByLocationIdFullyLoaded();
  const getMediaByLocationIdLazyLoadParams = makeMediaByLocationIdLazyLoadParams();

  const mapStateToProps = (state, ownProps) => ({
    mediaItems: getMediaByLocationIdItems(state, ownProps.locationId),
    isFetched: getMediaByLocationIdIsFetched(state, ownProps.locationId),
    fullyLoaded: getMediaByLocationIdFullyLoaded(state, ownProps.locationId),
    lazyLoadParams: getMediaByLocationIdLazyLoadParams(state, ownProps.locationId),
  });
  return mapStateToProps;
};

// mapDispatchToProps tells the container component how to dispatch actions to the redux store
const mapDispatchToProps = (dispatch, ownProps) => ({

  fetchMediaPage: (lazyLoadParams) => {

    const {
      params,
      paginationData,
    } = lazyLoadParams;
    const {
      page,
    } = paginationData;
    const { locationId } = params;

    console.log('LocationMediaListLazyLoad fetchMediaPage: ', locationId, page);
    dispatch(fetchMediaPageByTag(locationId, page));
  },
});

const LocationMediaListLazyLoad = connect(
  makeMapStateToProps,
  mapDispatchToProps,
)(MediaListLazyLoad);


export { LocationMediaListLazyLoad };
