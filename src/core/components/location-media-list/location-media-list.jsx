import { connect } from 'react-redux';
import { MediaList } from 'AppSrc/core/components/media-list/media-list';
import { fetchMediaByTag } from 'AppSrc/core/actions/actions-media-list';
import {
  makeMediaByLocationIdItems,
  makeMediaByLocationIdIsFetching,
  makeMediaByLocationIdIsFetched,
} from 'AppSrc/core/selectors/selectors-media-list';

/* --- Location Media List container component --- */

// MM: 'makeMapStateToProps' is so each instance of the container gets its own mapStateToProps
// otherwise the selectors that depend on props don't properly memoize
// https://redux.js.org/recipes/computing-derived-data#sharing-selectors-across-multiple-components
const makeMapStateToProps = () => {

  const getMediaByLocationIdItems = makeMediaByLocationIdItems();
  const getMediaByLocationIdIsFetching = makeMediaByLocationIdIsFetching();
  const getMediaByLocationIdIsFetched = makeMediaByLocationIdIsFetched();

  const mapStateToProps = (state, ownProps) => ({
    mediaItems: getMediaByLocationIdItems(state, ownProps.locationId),
    isFetching: getMediaByLocationIdIsFetching(state, ownProps.locationId),
    isFetched: getMediaByLocationIdIsFetched(state, ownProps.locationId),
  });
  return mapStateToProps;
};


// mapDispatchToProps tells the container component how to dispatch actions to the redux store
const mapDispatchToProps = (dispatch, ownProps) => ({
  fetchMedia: () => {
    dispatch(fetchMediaByTag(ownProps.locationId));
  },
});

const LocationMediaList = connect(
  makeMapStateToProps,
  mapDispatchToProps,
)(MediaList);


export { LocationMediaList };
