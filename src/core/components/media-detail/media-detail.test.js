import React from 'react';
import { mount } from 'enzyme';
import {
  TestRouter,
  rendersWithoutCrashing,
  matchesThePreviousSnapshot,
} from 'AppSrc/core/test/utils/component-test-utils';
import { testMediaDetail } from 'AppSrc/core/test/fixtures/media-detail-fixtures';
import { MediaDetailDisplay } from './media-detail';

jest.mock('AppSrc/core/components/loader/loader', () => (
  { Loader: 'mock-loader' }
));

jest.mock('AppSrc/core/components/breadcrumb/breadcrumb', () => (
  { Breadcrumb: 'mock-breadcrumb' }
));

const MediaDetailWithRouter = (
  <TestRouter>
    <MediaDetailDisplay
      media={testMediaDetail}
      isFetching={false}
      fetchMediaDetailData={()=> null}
      match={{params: {mediaId: testMediaDetail.id}, isExact: true, path: "", url: ""}}
    />
  </TestRouter>
);

const MediaDetailWithData = (
  <MediaDetailDisplay
    media={testMediaDetail}
    isFetching={false}
    fetchMediaDetailData={()=> null}
    match={{params: {mediaId: testMediaDetail.id}, isExact: true, path: "", url: ""}}
  />
);

const MediaDetailFetching = (
  <MediaDetailDisplay
    media={null}
    isFetching={true}
    fetchMediaDetailData={()=> null}
    match={{params: {mediaId: testMediaDetail.id}, isExact: true, path: "", url: ""}}
  />
);


describe('Media Detail Component', () => {
  rendersWithoutCrashing(MediaDetailWithRouter);
  matchesThePreviousSnapshot(MediaDetailWithData, {shallow: true, snapshotName: 'media-detail-with-data'});
  matchesThePreviousSnapshot(MediaDetailFetching, {shallow: true, snapshotName: 'media-detail-fetching'});
});
