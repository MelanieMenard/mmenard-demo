import React from 'react';
import { Link } from 'react-router-dom';
import { mediaItemPropTypesShape } from './media-proptypes';
import './media-item.css';


/* --- Media item presentational component --- */
const Media = React.memo(({ media }) => {

  const inlineStyle = {
    backgroundImage: `url(${media.image})`,
  };

  return (
    <li className="media-item">
      <Link to={`/media/${media.id}`} className="media-item-link">
        <div className="media-item-image" style={inlineStyle} />
        <div className="media-item-info">
          <p className="media-item-title">{media.title}</p>
        </div>
      </Link>
    </li>
  );
});

// defaultProps prevent crashes if missing data
Media.defaultProps = {
  media: {
    title: '',
    link: '',
    image: '',
  },
};

Media.propTypes = {
  media: mediaItemPropTypesShape,
};


export { Media };
