import PropTypes from 'prop-types';

/* Declare proptypes shapes in reusable const */
const mediaItemPropTypesShape = PropTypes.shape({
  id: PropTypes.string,
  title: PropTypes.string,
  secret: PropTypes.string,
  authorId: PropTypes.string,
  image: PropTypes.string,
});

export { mediaItemPropTypesShape };
