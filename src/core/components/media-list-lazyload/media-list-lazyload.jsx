import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Loader } from 'AppSrc/core/components/loader/loader';
import { ScrollTop } from 'AppSrc/core/components/scroll-top/scroll-top';
import { LazyLoader, lazyLoadParamsShape } from 'AppSrc/core/components/lazy-loader/lazy-loader';
import { mediaItemPropTypesShape } from 'AppSrc/core/components/media-item/media-proptypes';
import { Media } from 'AppSrc/core/components/media-item/media-item';
import { MEDIA_PER_PAGE } from 'AppSrc/core/queries/axios-queries';
import 'AppSrc/core/components/media-list/media-list.css';


/* --- Media list with lazyloader presentational component --- */


class MediaListLazyLoad extends PureComponent {

  constructor(props) {
    super(props);
    this.lazyLoadPage = this.lazyLoadPage.bind(this);
  }

  // used by lazyloader when a page needs to be fetched
  lazyLoadPage(lazyLoadParams) {
    this.props.fetchMediaPage(lazyLoadParams);
  }

  render() {

    const {
      mediaItems,
      lazyLoadParams,
      isFetched,
      fullyLoaded,
    } = this.props;

    const {
      isFetching,
    } = lazyLoadParams;

    const noMatchingItems = !isFetching && isFetched && !mediaItems.length;
    const userMessage = noMatchingItems ? 'No matching images found.' : '';

    const showScrollTop = fullyLoaded && !noMatchingItems && (mediaItems.length > MEDIA_PER_PAGE);

    return (
      <LazyLoader
        lazyLoad={this.lazyLoadPage}
        lazyLoadParams={lazyLoadParams}
        fullyLoaded={fullyLoaded}
        bottomOffset={150}
      >
        <div className="media-list-wrapper">
          {userMessage && (
            <div className="message">
              <p>{userMessage}</p>
            </div>
          )}
          {(!userMessage) && (
            <ul className="media-list">
              {mediaItems.map((media) => (
                <Media
                  key={media.id}
                  media={media}
                />
              ))}
            </ul>
          )}
          {isFetching && (
            <Loader />
          )}
          {showScrollTop && (
            <ScrollTop />
          )}
        </div>
      </LazyLoader>
    );
  }
}

MediaListLazyLoad.propTypes = {
  mediaItems: PropTypes.arrayOf(mediaItemPropTypesShape),
  fetchMediaPage: PropTypes.func,
  lazyLoadParams: lazyLoadParamsShape,
  isFetched: PropTypes.bool,
  fullyLoaded: PropTypes.bool,
};

// defaultProps prevent crashes if missing data
MediaListLazyLoad.defaultProps = {
  mediaItems: [],
};


export { MediaListLazyLoad };
