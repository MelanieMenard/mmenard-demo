import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { Loader } from 'AppSrc/core/components/loader/loader';
import { ScrollTop } from 'AppSrc/core/components/scroll-top/scroll-top';
import { mediaItemPropTypesShape } from 'AppSrc/core/components/media-item/media-proptypes';
import { Media } from 'AppSrc/core/components/media-item/media-item';
import { MEDIA_PER_PAGE } from 'AppSrc/core/queries/axios-queries';
import './media-list.css';


/* --- Media list presentational component --- */
const MediaList = React.memo(({
  mediaItems,
  isFetching,
  isFetched,
  fetchMedia,
}) => {

  // fetch the media if not already doing so or already cached
  useEffect(
    () => {
      if (!isFetching && !isFetched) {
        fetchMedia();
      }
    },
    [isFetching, isFetched, fetchMedia],
  );


  // show spinner if we are fetching and no items yet returned (otherwise show what is there and update as more get fetched)
  const showSpinner = isFetching && !mediaItems.length;
  const noMatchingItems = isFetched && !mediaItems.length;
  const userMessage = noMatchingItems ? 'No matching images found.' : '';
  const showScrollTop = (mediaItems.length > MEDIA_PER_PAGE);

  return (
    <div className="media-list-wrapper">

      {showSpinner && (
        <Loader />
      )}
      {userMessage && (
        <div className="message">
          <p>{userMessage}</p>
        </div>
      )}
      {(!showSpinner && !userMessage) && (
        <ul className="media-list">
          {mediaItems.map((media) => (
            <Media
              key={media.id}
              media={media}
            />
          ))}
        </ul>
      )}
      {showScrollTop && (
        <ScrollTop />
      )}

    </div>
  );
});


// defaultProps prevent crashes if missing data
MediaList.defaultProps = {
  mediaItems: [],
};

MediaList.propTypes = {
  mediaItems: PropTypes.arrayOf(mediaItemPropTypesShape),
  isFetching: PropTypes.bool,
  isFetched: PropTypes.bool,
  fetchMedia: PropTypes.func,
};


export { MediaList };
