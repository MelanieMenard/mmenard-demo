import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { setSelectedTag } from 'AppSrc/core/actions/actions-tags';
import {
  makeGetTagById,
  makeIsTagSelected,
} from 'AppSrc/core/selectors/selectors-tags';
import { locationPropTypesShape } from 'AppSrc/core/data-structures/location-proptypes';
import './tag-cloud.css';


/* --- Tag item presentational component --- */
const TagDisplay = React.memo(({
  tag,
  tagId,
  isSelected,
  selectTag,
}) => {

  const matchingItemsString = (tag.matchingItems) ? ` (${tag.matchingItems})` : '';

  return (
    <li className={`tag-item popularity-${tag.popularity}${isSelected ? ' selected' : ''}`}>
      <button
        type="button"
        className="tag"
        onClick={selectTag}
      >
        <p className="tag-title">
          {tag.displayName}
          {matchingItemsString}
        </p>
      </button>
    </li>
  );
});

// defaultProps prevent crashes if missing data
TagDisplay.defaultProps = {
  tag: { displayName: '' },
};

TagDisplay.propTypes = {
  tag: locationPropTypesShape,
  tagId: PropTypes.string,
  isSelected: PropTypes.bool,
  selectTag: PropTypes.func,
};


/* --- Tag item container component --- */

// MM: 'makeMapStateToProps' is so each instance of the container gets its own mapStateToProps
// otherwise the selectors that depend on props don't properly memoize
// https://redux.js.org/recipes/computing-derived-data#sharing-selectors-across-multiple-components
const makeMapStateToProps = () => {

  const getTagById = makeGetTagById();
  const isTagSelected = makeIsTagSelected();

  const mapStateToProps = (state, ownProps) => ({
    tag: getTagById(state, ownProps.tagId),
    isSelected: isTagSelected(state, ownProps.tagId),
  });
  return mapStateToProps;
};


// mapDispatchToProps tells the container component how to dispatch actions to the redux store
const mapDispatchToProps = (dispatch, ownProps) => ({
  // Set selected tag
  selectTag: () => {
    dispatch(setSelectedTag(ownProps.tagId));
  },
});

const Tag = connect(
  makeMapStateToProps,
  mapDispatchToProps,
)(TagDisplay);


// 'Display' unconnected presentational components are exported for testing only
export {
  TagDisplay,
  Tag,
};
