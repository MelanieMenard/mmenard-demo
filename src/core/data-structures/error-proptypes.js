import PropTypes from 'prop-types';

// standardised error
const errorPropTypesShape = PropTypes.shape({
  code: PropTypes.number,
  message: PropTypes.string,
});

export { errorPropTypesShape };
