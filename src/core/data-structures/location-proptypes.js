import PropTypes from 'prop-types';

/* Declare proptypes shapes in reusable const */
const locationPropTypesShape = PropTypes.shape({
  id: PropTypes.string,
  displayName: PropTypes.string,
  searchQuery: PropTypes.string,
  description: PropTypes.string,
  type: PropTypes.string,
  latitude: PropTypes.number,
  longitude: PropTypes.number,
  matchingItems: PropTypes.number,
  popularity: PropTypes.number,
  popularityWideScale: PropTypes.number,
});

// we expect the default other key to be here
// other keys depend on the data, we can't guess them
const colorMapPropTypesShape = PropTypes.shape({
  other: PropTypes.string,
});

export {
  locationPropTypesShape,
  colorMapPropTypesShape,
};
