/* ------------------------------------------- */
/*   Axios instance that reused accross queries for efficiency
/*   Generic REST query functions making use of the instance, used in the async thunk actions
/* ------------------------------------------- */

import axios from 'axios';
import http from 'http';
import https from 'https';
import { AppCredentials } from 'AppSrc/core/auth/auth';
import {
  catchHiddenError,
  handleError,
} from 'AppSrc/core/queries/error-handler';


const MEDIA_PER_PAGE = 20;


// First you set global config defaults that will be applied to every instances and requests
// https://github.com/axios/axios#global-axios-defaults
// 50 sec timeout
axios.defaults.timeout = 50000;
// follow up to 10 HTTP 3xx redirects
axios.defaults.maxRedirects = 10;
// cap the maximum content length we'll accept to 50MBs, just in case
axios.defaults.maxContentLength = 50 * 1000 * 1000;
// keepAlive pools and reuses TCP connections, so it's faster
axios.defaults.httpAgent = new http.Agent({ keepAlive: true });
axios.defaults.httpsAgent = new https.Agent({ keepAlive: true });


// then you create instance specific config when creating the instance
// https://github.com/axios/axios#custom-instance-defaults
// if we had authorization headers we would set them here
const axiosInstance = axios.create();


// utility function for posting a REST GET query to axios
// includes handling of 200 responses with hidden errors
// and standard error responses
// returns a promise with the response
// for REST queries, the endpoint depends on the data, so getRESTQuery gets it as an argument along with the query
// so this utility function is reusable for any REST data
const getRESTQuery = (endpoint, query) => axiosInstance.get(endpoint, { params: query })
  .then((response) => catchHiddenError(response))
  .catch((error) => handleError(error));


// GET query to Flickr Public Feed API via axios
// returns a promise with the response
// !!! this feed only returns jsonp, which works fine with jquery but not axios!
// so we need to use the public search instead
const getFlickrFeedQuery = (searchString) => {

  // Flickr Public Feed API url
  // https://www.flickr.com/services/feeds/docs/photos_public/
  const endpoint = 'https://api.flickr.com/services/feeds/photos_public.gne';

  const formattedQueryTerms = searchString.split(' ').join(',');
  // nojsoncallback necessary to make the API return plain JSON rather than JSONP
  const query = {
    tags: formattedQueryTerms,
    format: 'json',
    nojsoncallback: 'true',
  };

  return getRESTQuery(endpoint, query)
    .then((response) => {
      // modify the image filename in each image result as Flickr returns tiny thumbnails
      const formattedResponse = response.data.items.map((item) => {
        const formattedResult = {
          title: item.title,
          author: item.author,
          link: item.link,
          image: item.media.m.replace('_m.jpg', '.jpg'),
        };
        return formattedResult;
      });
      return formattedResponse;
    });

};


// GET query to Flickr Photo Search API via axios
// returns a promise with the response
const getFlickrSearchQuery = (searchString, page = 1, perPage = MEDIA_PER_PAGE) => {

  // Flickr Photo Search API url
  // https://www.flickr.com/services/api/flickr.photos.search.html
  const endpoint = 'https://api.flickr.com/services/rest/';

  const formattedQueryTerms = searchString.split(' ').join(',');
  // nojsoncallback necessary to make the API return plain JSON rather than JSONP
  const query = {
    page,
    method: 'flickr.photos.search',
    api_key: AppCredentials.Flickr.key,
    tags: formattedQueryTerms,
    // tags: searchString,
    tag_mode: 'all',
    sort: 'interestingness-desc',
    per_page: perPage,
    format: 'json',
    nojsoncallback: 'true',
  };

  return getRESTQuery(endpoint, query)
    .then((response) => {
      const formattedResponse = {
        total: response.data.photos.total,
        pages: response.data.photos.pages,
        page: response.data.photos.page,
        perPage: response.data.photos.perpage,
      };

      // work out the media url from Flickr search info
      // https://www.flickr.com/services/api/misc.urls.html
      formattedResponse.items = response.data.photos.photo.map((item) => {
        const formattedResult = {
          title: item.title,
          id: item.id,
          secret: item.secret,
          authorId: item.owner,
          image: `https://farm${item.farm}.staticflickr.com/${item.server}/${item.id}_${item.secret}_z.jpg`,
        };
        return formattedResult;
      });
      return formattedResponse;
    });
};


// GET query to Flickr Photo Info API via axios
// returns a promise with the response
const getFlickrPhotoInfoQuery = (photoId) => {

  // Flickr Photo Info API url
  // https://www.flickr.com/services/api/flickr.photos.getInfo.html
  const endpoint = 'https://api.flickr.com/services/rest/';

  // nojsoncallback necessary to make the API return plain JSON rather than JSONP
  const query = {
    method: 'flickr.photos.getInfo',
    api_key: AppCredentials.Flickr.key,
    photo_id: photoId,
    format: 'json',
    nojsoncallback: 'true',
  };

  return getRESTQuery(endpoint, query)
    .then((response) => {
      const photoData = response.data.photo;
      // flickr starts its key name by underscores!
      /* eslint-disable no-underscore-dangle */
      const formattedPhotoData = {
        id: photoData.id,
        secret: photoData.secret,
        link: `https://farm${photoData.farm}.staticflickr.com/${photoData.server}/${photoData.id}_${photoData.secret}_b.jpg`,
        title: photoData.title._content,
        description: photoData.description._content,
        author: photoData.owner.username,
        dateTaken: photoData.dates.taken,
      };
      return formattedPhotoData;
    });
};


export {
  axiosInstance,
  MEDIA_PER_PAGE,
  getFlickrFeedQuery,
  getFlickrSearchQuery,
  getFlickrPhotoInfoQuery,
};

