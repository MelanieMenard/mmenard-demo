import {
  errorDefaultState,
  errorReducer,
} from 'AppSrc/core/reducers/reducer-error';
import { REMOVE_ERROR_DISPLAYED } from 'AppSrc/core/actions/actions-error';
import { FETCH_MEDIA_LIST_ERROR } from 'AppSrc/core/actions/actions-media-list';
import { 
  expectStateToHaveChanges,
  testDefaultReducerBehaviour,
} from 'AppSrc/core/test/utils/reducer-test-utils';
import {
  testError,
  testError1,
  testError2,
} from 'AppSrc/core/test/fixtures/error-fixture';



const testAddErrorToArray = () => {

  describe('it appends an error to the array if the same eror code is not already in', () => {

    const initialState = {
      ...errorDefaultState,
      errors: [
        ...errorDefaultState.errors,
        testError1,
      ]
    };
    const action = {
      type: FETCH_MEDIA_LIST_ERROR,
      payload: {
        tagId: 'monksHouse',
        page: 1,
        error: testError2,
      }
    };
    const outputState = errorReducer(initialState, action);
    expectStateToHaveChanges(
      outputState,
      initialState,
      {
        errors: [
          ...initialState.errors,
          testError2,
        ]
      }
    );
  });

  describe('it does not append an error to the array if the same eror code is already in', () => {

    const initialState = {
      ...errorDefaultState,
      errors: [
        ...errorDefaultState.errors,
        testError1,
        testError2,
      ]
    };
    const action = {
      type: FETCH_MEDIA_LIST_ERROR,
      payload: {
        tagId: 'monksHouse',
        page: 1,
        error: testError2,
      }
    };
    const outputState = errorReducer(initialState, action);
    expectStateToHaveChanges(
      outputState,
      initialState,
      {}
    );
  });
}

const testRemoveErrorDisplayed = () => {

  describe('REMOVE_ERROR_DISPLAYED removes the first error in the array', () => {

    const initialState = {
      ...errorDefaultState,
      errors: [
        ...errorDefaultState.errors,
        testError1,
        testError2,
      ]
    };
    const action = {
      type: REMOVE_ERROR_DISPLAYED,
      payload: {
      }
    };
    const outputState = errorReducer(initialState, action);
    expectStateToHaveChanges(
      outputState,
      initialState,
      {
        errors: [
          testError2,
        ]
      }
    );
  });
}


describe('errorReducer', () => {
  testDefaultReducerBehaviour(errorReducer, errorDefaultState);
  testAddErrorToArray();
  testRemoveErrorDisplayed();
});
