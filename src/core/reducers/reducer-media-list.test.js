import {
  mediaListReducerDefaultState,
  mediaListReducer,
} from 'AppSrc/core/reducers/reducer-media-list';
import {
  FETCH_MEDIA_LIST_REQUEST,
  FETCH_MEDIA_LIST_SUCCESS,
  FETCH_MEDIA_LIST_ERROR,
} from 'AppSrc/core/actions/actions-media-list';
import { 
  expectStateToHaveChanges,
  testDefaultReducerBehaviour,
} from 'AppSrc/core/test/utils/reducer-test-utils';
import {
  mediaListPage1,
  mediaListPage2,
  mediaListPage2Refreshed,
  mediaListPage3,
  TOTAL_MEDIA_ITEMS_ON_SERVER,
  TOTAL_MEDIA_PAGES_ON_SERVER,
  MEDIA_PER_PAGE_FIXTURE,
} from 'AppSrc/core/test/fixtures/media-list-fixtures';
import { testError } from 'AppSrc/core/test/fixtures/error-fixture';


const testfetchMediaListRequest = () => {

  describe('it sets isFetching to true  on the corresponding tag on fetch request', () => {

    const action = {
      type: FETCH_MEDIA_LIST_REQUEST,
      payload: {
        tagId: 'monksHouse',
        page: 1,
      }
    };
    const outputState = mediaListReducer(mediaListReducerDefaultState, action);
    expectStateToHaveChanges(
      outputState,
      mediaListReducerDefaultState,
      {
        mediaByLocation: {
          ...mediaListReducerDefaultState.mediaByLocation,
          ['monksHouse']: {
            ...mediaListReducerDefaultState.mediaByLocation['monksHouse'],
            isFetching: true,
          },
        },
      }
    );
  });
}

const testfetchMediaListError = () => {

  describe('it sets isFetching to false  on the corresponding tag on fetch error', () => {

    const initialState = {
      ...mediaListReducerDefaultState,
      mediaByLocation: {
        ...mediaListReducerDefaultState.mediaByLocation,
        ['monksHouse']: {
          ...mediaListReducerDefaultState.mediaByLocation['monksHouse'],
          isFetching: true,
        },
      },
    };
    const action = {
      type: FETCH_MEDIA_LIST_ERROR,
      payload: {
        tagId: 'monksHouse',
        page: 1,
        error: testError,
      }
    };
    const outputState = mediaListReducer(initialState, action);
    expectStateToHaveChanges(
      outputState,
      initialState,
      {
        mediaByLocation: {
          ...mediaListReducerDefaultState.mediaByLocation,
          ['monksHouse']: {
            ...mediaListReducerDefaultState.mediaByLocation['monksHouse'],
            isFetching: false,
          },
        },
      }
    );
  });
}

const testfetchMediaListSuccess = () => {

  describe('it puts the correct pagination data on fetch success for the first page', () => {

    const action = {
      type: FETCH_MEDIA_LIST_SUCCESS,
      payload: {
        tagId: 'monksHouse',
        items: mediaListPage1,
        totalItems: TOTAL_MEDIA_ITEMS_ON_SERVER,
        currentPage: 1,
        totalPages: TOTAL_MEDIA_PAGES_ON_SERVER,
        perPage: MEDIA_PER_PAGE_FIXTURE,
      }
    };
    const initialState = {
      ...mediaListReducerDefaultState,
      mediaByLocation: {
        ...mediaListReducerDefaultState.mediaByLocation,
        ['monksHouse']: {
          ...mediaListReducerDefaultState.mediaByLocation['monksHouse'],
          isFetching: false,
        },
      },
    };
    const outputState = mediaListReducer(initialState, action);
    expectStateToHaveChanges(
      outputState,
      initialState,
      {
        mediaByLocation: {
          ...initialState.mediaByLocation,
          ['monksHouse']: {
            ...initialState.mediaByLocation['monksHouse'],
            isFetched: true,
            isFetching: false,
            total: TOTAL_MEDIA_ITEMS_ON_SERVER,
            currentPage: 1,
            totalPages: TOTAL_MEDIA_PAGES_ON_SERVER,
            perPage: MEDIA_PER_PAGE_FIXTURE,
            items: [
              ...mediaListPage1,
            ],
          },
        },
      }
    );
  });

  describe('it puts the correct pagination data and appends subsequent pages on fetch success', () => {

    const action = {
      type: FETCH_MEDIA_LIST_SUCCESS,
      payload: {
        tagId: 'monksHouse',
        items: mediaListPage2,
        totalItems: TOTAL_MEDIA_ITEMS_ON_SERVER,
        currentPage: 2,
        totalPages: TOTAL_MEDIA_PAGES_ON_SERVER,
        perPage: MEDIA_PER_PAGE_FIXTURE,
      }
    };
    const initialState = {
      ...mediaListReducerDefaultState,
      mediaByLocation: {
        ...mediaListReducerDefaultState.mediaByLocation,
        ['monksHouse']: {
          ...mediaListReducerDefaultState.mediaByLocation['monksHouse'],
          isFetched: true,
          isFetching: false,
          total: TOTAL_MEDIA_ITEMS_ON_SERVER,
          currentPage: 1,
          totalPages: TOTAL_MEDIA_PAGES_ON_SERVER,
          perPage: MEDIA_PER_PAGE_FIXTURE,
          items: [
            ...mediaListPage1,
          ]
        },
      },
    };
    const outputState = mediaListReducer(initialState, action);
    expectStateToHaveChanges(
      outputState,
      initialState,
      {
        mediaByLocation: {
          ...initialState.mediaByLocation,
          ['monksHouse']: {
            ...initialState.mediaByLocation['monksHouse'],
            isFetched: true,
            isFetching: false,
            total: TOTAL_MEDIA_ITEMS_ON_SERVER,
            currentPage: 2,
            totalPages: TOTAL_MEDIA_PAGES_ON_SERVER,
            perPage: MEDIA_PER_PAGE_FIXTURE,
            items: [
              ...mediaListPage1,
              ...mediaListPage2,
            ],
          },
        },
      }
    );
  });

  describe('it refreshes a page without affecting the other pages around it on fetch success', () => {

    const action = {
      type: FETCH_MEDIA_LIST_SUCCESS,
      payload: {
        tagId: 'monksHouse',
        items: mediaListPage2Refreshed,
        totalItems: TOTAL_MEDIA_ITEMS_ON_SERVER,
        currentPage: 2,
        totalPages: TOTAL_MEDIA_PAGES_ON_SERVER,
        perPage: MEDIA_PER_PAGE_FIXTURE,
      }
    };
    const initialState = {
      ...mediaListReducerDefaultState,
      mediaByLocation: {
        ...mediaListReducerDefaultState.mediaByLocation,
        ['monksHouse']: {
          ...mediaListReducerDefaultState.mediaByLocation['monksHouse'],
          isFetched: true,
          isFetching: false,
          total: TOTAL_MEDIA_ITEMS_ON_SERVER,
          currentPage: 3,
          totalPages: TOTAL_MEDIA_PAGES_ON_SERVER,
          perPage: MEDIA_PER_PAGE_FIXTURE,
          items: [
            ...mediaListPage1,
            ...mediaListPage2,
            ...mediaListPage3,
          ]
        },
      },
    };
    const outputState = mediaListReducer(initialState, action);
    expectStateToHaveChanges(
      outputState,
      initialState,
      {
        mediaByLocation: {
          ...initialState.mediaByLocation,
          ['monksHouse']: {
            ...initialState.mediaByLocation['monksHouse'],
            isFetched: true,
            isFetching: false,
            total: TOTAL_MEDIA_ITEMS_ON_SERVER,
            currentPage: 2,
            totalPages: TOTAL_MEDIA_PAGES_ON_SERVER,
            perPage: MEDIA_PER_PAGE_FIXTURE,
            items: [
              ...mediaListPage1,
              ...mediaListPage2Refreshed,
              ...mediaListPage3,
            ],
          },
        },
      }
    );
  });


}


describe('mediaListReducer', () => {
  testDefaultReducerBehaviour(mediaListReducer, mediaListReducerDefaultState);
  testfetchMediaListRequest();
  testfetchMediaListError();
  testfetchMediaListSuccess();
});
