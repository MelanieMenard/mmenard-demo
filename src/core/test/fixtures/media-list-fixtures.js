const media1 = {
  id: 'mediaId1',
  title: 'media1 title',
  image: 'www.link.com/media1.jpg',
};

const media2 = {
  id: 'mediaId2',
  title: 'media2 title',
  image: 'www.link.com/media2.jpg',
};

const media3 = {
  id: 'mediaId3',
  title: 'media3 title',
  image: 'www.link.com/media3.jpg',
};

const media4 = {
  id: 'mediaId4',
  title: 'media4 title',
  image: 'www.link.com/media4.jpg',
};

const media5 = {
  id: 'mediaId5',
  title: 'media5 title',
  image: 'www.link.com/media5.jpg',
};

const media5Refreshed = {
  id: 'mediaId5',
  title: 'media5 title UPDATED',
  image: 'www.link.com/media5.jpg',
};

const media6 = {
  id: 'mediaId6',
  title: 'media6 title',
  image: 'www.link.com/media6.jpg',
};

const media7 = {
  id: 'mediaId7',
  title: 'media7 title',
  image: 'www.link.com/media7.jpg',
};

const media8 = {
  id: 'mediaId8',
  title: 'media8 title',
  image: 'www.link.com/media8.jpg',
};

const mediaListPage1 = [
  media1,
  media2,
  media3, 
];

const mediaListPage2 = [
  media4,
  media5,
  media6, 
];

const mediaListPage2Refreshed = [
  media4,
  media5Refreshed,
  media6, 
];

const mediaListPage3 = [
  media7,
  media8,
];

// smaller pages for testing so I don'thave to make huge fixtures
const MEDIA_PER_PAGE_FIXTURE = 3;
// fake a response from the API
const TOTAL_MEDIA_ITEMS_ON_SERVER = 8;
const TOTAL_MEDIA_PAGES_ON_SERVER = 3;

export {
  media1,
  mediaListPage1,
  mediaListPage2,
  mediaListPage2Refreshed,
  mediaListPage3,
  TOTAL_MEDIA_ITEMS_ON_SERVER,
  TOTAL_MEDIA_PAGES_ON_SERVER,
  MEDIA_PER_PAGE_FIXTURE,
};
