/****************************************************
  Utility function to test Redux reducers 
/*****************************************************/

// test default reducer behaviour
// - returns default state with input state
// - leaves state untouched if no action
// - leaves state untouched if action not recognised
// parameters: the reducer to test and the reducers default state
const testDefaultReducerBehaviour = (reducer, defaultState) => {
  /** behaviour with no action **/
  describe('default behaviour', () => {
    it('returns the default state',() => {
      expect(reducer(undefined, {})).toEqual(defaultState);
    });

    // a state to pass in
      const stateParam = {
        a: 1,
        b: 2,
        c: 'some value'
      }

    it('returns passed in state with no action',() => {
      expect(reducer(stateParam, {})).toEqual(stateParam);
    });

    // an unrecognised action
    const unrecognisedAction = {
      type: 'UNRECOGNISED_ACTION',
      payload: {thing: 'value'}
    };

    it('returns passed in state with unrecognised action',() => {
      expect(reducer(stateParam, unrecognisedAction)).toEqual(stateParam);
    });
  });
}

/*
  Utility function to provide explicit expects for each key changed in state
  And a further expect for the rest of state maintaining.
  For each key an `it` will be run with the description
  "set's value of <key> to <value>", with an expect that. checks the changed state.
  A further test will be run to check the rest of state is maintained
  parameters:
  changedState: the output state that should have changed
  initialState: the initial state before the state change
  expectedChanges: The changes expected to be reflected in the new state
*/
const expectStateToHaveChanges = (changedState, initialState, expectedChanges) => {
  const changedKeys = Object.keys(expectedChanges);
  changedKeys.forEach(key => {
    const value = expectedChanges[key];
    // If value is an object, the message should say 'Sets value of <key>'
    // as the stringified object value can be very long
    // Otherwise use the more explicit message of 'Sets <key> to <value>'
    const message = value instanceof Object ? (
      `sets value of ${key}` 
    ) : (
      `sets ${key} to ${JSON.stringify(value)}`
    );
    // test that each key has been set to the expected value
    it(message,() => {
      expect(changedState[key]).toEqual(value);
    });
  });
  // Add a further test to show that the rest of state was maintained
  it('maintains the rest of state', () => {
    expect(changedState).toEqual({
      ...initialState,
      ...expectedChanges
    });
  });
}

export  {
  testDefaultReducerBehaviour,
  expectStateToHaveChanges,
};
