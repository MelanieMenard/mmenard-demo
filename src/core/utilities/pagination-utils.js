/* - paginatedArray: utility used by reducer to concatenate pages of items fetched from server into 1 array - */
// existingItems: array of items already in redux
// fetchedItems:new page of items fetched form the server
// totalItemsCount: number of total items to fetch from server
// offset: index of item we want to concat from
// perPage: items per page

const paginatedArray = (existingItems, fetchedItems, totalItemsCount, offset, perPage) => {

  const existingItemsCount = existingItems.length;
  const fetchedItemsCount = fetchedItems.length;

  const endIndex = offset + fetchedItemsCount - 1;

  // find out what type of page has been fetched?
  // is it items never fetched before ?
  const isNextItems = (offset === existingItemsCount);
  // is it refresh of existing items in the middle of the array?
  const isRefresh = ((offset < existingItemsCount) && (endIndex < existingItemsCount));
  // special case of refreshing the last page with also items appended or removed at the end
  const endItemsAdded = (endIndex >= existingItemsCount);
  const endItemsRemoved = (totalItemsCount < existingItemsCount);
  const isUpdateLastPage = ((offset < existingItemsCount) && (endItemsAdded || endItemsRemoved));


  // append next items to end of existing array
  if (isNextItems) {

    const data = {
      paginationCase: 'isNextItems',
      paginatedItems: [
        ...existingItems,
        ...fetchedItems,
      ],
    };

    return data;
  }
  // replace refreshed items in array
  if (isRefresh && !isUpdateLastPage) {

    const data = {
      paginationCase: 'isRefresh',
      paginatedItems: [
        ...existingItems.slice(0, offset),
        ...fetchedItems,
        ...existingItems.slice(endIndex + 1),
      ],
    };

    return data;
  }
  // special case of refreshing the last page with also items appended or removed at the end
  if (isUpdateLastPage) {

    const data = {
      paginationCase: 'isUpdateLastPage',
      paginatedItems: [
        ...existingItems.slice(0, offset),
        ...fetchedItems,
      ],
    };

    return data;
  }

  // otherwise the pagination order got completely lost ??? What to do? replace the array since the new items are what we want to see?
  const data = {
    paginationCase: 'paginationGotLost',
    paginatedItems: [
      ...fetchedItems,
    ],
  };
  return data;
};

export { paginatedArray };
